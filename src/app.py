from __future__ import print_function

import redis as redis
from celery import Celery
from time import sleep

from redis import StrictRedis

import pickle

redis = StrictRedis('redis')


class Config(object):
    broker_url = 'redis://redis:6379/0'
    result_backend = 'redis://redis:6379/0'
    task_serializer = 'pickle'
    result_serializer = 'pickle'
    accept_content = {'pickle'}

app = Celery('app')
app.config_from_object(Config)


@app.task
def dependent_taskA(i):
    return i


@app.task(rate_limit='5/m')
def dependent_taskB(i):
    return i + 1


@app.task
def my_task():
    i = 0
    print(res.id)
    while True:
        sleep(3)
        (dependent_taskA.s() | dependent_taskB.s())(i)
        res_ = redis.get('celery-task-meta-' + res.id)
        if res_:
            print(pickle.loads(res_))
        i += 1

res = dependent_taskA.delay(5)
my_task.delay()
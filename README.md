Repository includes a distributable, isolated Docker installation to test our academic exploit of 
historical Redis vulnerabilities for our Graduate-Level Computer Security course.

[Install Docker](https://docs.docker.com/engine/installation/)
[Install Docker-Compose](https://docs.docker.com/compose/install/)


Setup your redis with some data:

```
# this starts the "main" redis process
docker-compose up redis
# this connects a cli to the main process and sets a value
docker-compose run redis redis-cli -h redis set abc "my important data"
```

In another tab run:

```
docker-compose run pwn
```

In the original tab:

```
docker-compose run redis redis-cli -h redis get abc
"129"
```

Shutdown everything:

```
docker-compose down
```